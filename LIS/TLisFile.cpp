#include "TLisFile.h"
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QDebug>
#include "TLisInterface.h"

TLisFile::TLisFile(QObject *parent) : QObject(parent)
{
}

void TLisFile::setFilePath(QString filepath)
{
    fPath = filepath;
    if (!fPath.isEmpty())
    {
        QDir dir;
        dir.mkpath(fPath);
    }
}

void TLisFile::write(lis_IdCode_t id, lis_Data_t *data)
{
    QString file = fPath;
    if (!fPath.isEmpty())
        file += '/';
    file += QDateTime::currentDateTime().toString("dd.MM.yy-hhmmss") + "_";
    switch(id)
    {
    default:
        return;

    case lisid_Workorder:
        file += tr("workorder-%1.txt").arg(data->Workorder.SampleId);
        break;

    case lisid_Result:
        file += tr("result-%1.txt").arg(data->Result.SampleId);
        break;
    }
    write(file, id, data);
}

void TLisFile::write(QString FullFileName, lis_IdCode_t id, lis_Data_t *data)
{
    QFile file(FullFileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        emit error(tr("запись PDU: %1").arg(FullFileName), file.errorString());
        return;
    }

    LIS_DATA buf[LIS_MAX_BUFSIZE];
    LIS_SIZE size = lis_write_data(id, data, buf, LIS_MAX_BUFSIZE);
    if (size < 0)
    {
        emit error(tr("запись PDU: %1").arg(FullFileName), TLisInterface::errorString(size));
        return;
    }

    int res = file.write((char*)buf, size);
    if ((res == -1)||(res != size))
        emit error(tr("запись PDU: %1").arg(FullFileName), file.errorString());
}

bool TLisFile::read(QString FullFileName, lis_IdCode_t id, lis_Data_t *data)
{
    QFile file(FullFileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        emit warning(tr("чтение PDU: %1").arg(FullFileName), file.errorString());
        return false;
    }

    QByteArray buf = file.readAll();
    LIS_SIZE res = lis_read_data((LIS_DATA*)buf.data(), buf.size(), id, data);
    if (res < 0)
    {
        emit warning(tr("чтение PDU: %1").arg(FullFileName), TLisInterface::errorString(res));
        return false;
    }
    return true;
}

/*!
 * \file lis_codetable.h
 * \author Андрей Шиганцов
 * \brief Таблица кодов реализации кодирования по протоколу LIS
 */

 #ifndef __LIS_CODE_TABLE__
 #define __LIS_CODE_TABLE__

#define LIS_NUL  0x00u
#define LIS_SOH  0x01u
#define LIS_STX  0x02u
#define LIS_ETX  0x03u
#define LIS_EOT  0x04u
#define LIS_ENQ  0x05u
#define LIS_ACK  0x06u
#define LIS_BEL  0x07u
#define LIS_BS	 0x08u
#define LIS_HT	 0x09u
#define LIS_LF	 0x0au
#define LIS_VT	 0x0bu
#define LIS_FF	 0x0cu
#define LIS_CR	 0x0du
#define LIS_SO	 0x0eu
#define LIS_SI	 0x0fu
#define LIS_DLE	 0x10u
#define LIS_DC1	 0x11u
#define LIS_DC2  0x12u
#define LIS_DC3	 0x13u
#define LIS_DC4	 0x14u
#define LIS_NACK 0x15u
#define LIS_SYN  0x16u
#define LIS_ETB  0x17u
#define LIS_CAN  0x18u
#define LIS_EM   0x19u
#define LIS_SUB  0x1au
#define LIS_ESC  0x1bu
#define LIS_FS   0x1cu
#define LIS_GS   0x1du
#define LIS_RS   0x1eu
#define LIS_US   0x1fu
#define LIS_SP   0x20u
//-----------------------
#define LIS_ALTMOD 0x7du
#define LIS_RUBOUT 0x7fu
//-----------------------
#define LIS_DEFAULT_LRC 0x7fu

#endif//__LIS_CODE_TABLE__

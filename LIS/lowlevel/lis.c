/*!
 * \file lis.c
 * \author Андрей Шиганцов
 * \brief Реализация кодирования по протоколу LIS
 */

#include "lis.h"
#include "lis_codetable.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define CHECK(assert, err) do{LIS_ASSERT(assert); if(!(assert)) return err;}while(0)

static LIS_DATA calc_lrc(LIS_DATA* buf, LIS_SIZE size)
{
    LIS_DATA lrc = 0;

    int i;
    for (i=0; i < size; i++)
        lrc ^= buf[i];
    if (lrc == LIS_ETX)
        lrc = LIS_DEFAULT_LRC;
    return lrc;
}

char lis_id_char(lis_IdCode_t id)
{
    switch (id)
    {
    default: return 0;
    case lisid_Initialization: return 'I';
    case lisid_TokenTransfer: return 'S';
    case lisid_Workorder: return 'Y';
    case lisid_WorkorderValidation: return 'E';
    case lisid_Result: return 'R';
    case lisid_Query: return 'Q';
    case lisid_NoWorkorder: return 'N';
    case lisid_ResultValidation: return 'Z';
    }
}

lis_IdCode_t lis_char_id(char data)
{
    switch (data)
    {
    default: return invalid_lisid;
    case 'I': return lisid_Initialization;
    case 'S': return lisid_TokenTransfer;
    case 'Y': return lisid_Workorder;
    case 'E': return lisid_WorkorderValidation;
    case 'R': return lisid_Result;
    case 'Q': return lisid_Query;
    case 'N': return lisid_NoWorkorder;
    case 'Z': return lisid_ResultValidation;
    }
}

//--------------------------

static inline void write_byte(LIS_DATA data, LIS_DATA* buf, LIS_SIZE* size)
{
    buf[(*size)++] = data;
}

static void write_string(const char* str, LIS_DATA* buf, LIS_SIZE* size)
{
    LIS_SIZE n = strlen(str);
    memcpy(buf+(*size), str, n);
    *size += n;
}

static void write_spaces(int count, LIS_DATA* buf, LIS_SIZE* size)
{
    int i;
    for (i = 0; i < count; i++)
        write_byte(' ', buf, size);
}

static int write(const char* format, LIS_DATA* buf, LIS_SIZE* size, ...)
{
    va_list args;
    va_start(args, size);
    int res = vsprintf((char*)buf+(*size), format, args);
    if (res > 0)
        *size += res;
    va_end(args);
    return res;
}

#define CHECK_BUFSIZE CHECK((size < bufsize), liserr_BufOverflow)
#define CHECK_WRITE_RES(res) CHECK((res > 0), liserr_WriteFailure)
int lis_write_data(lis_IdCode_t id, lis_Data_t* data, LIS_DATA* buf, LIS_SIZE bufsize)
{
    LIS_SIZE size = 0;
    int res = 0, i = 0;
    CHECK_BUFSIZE;
    switch(id)
    {
    default:
        LIS_ASSERT(0);
        return liserr_General;

    case lisid_Initialization:
        break;

    case lisid_TokenTransfer:
        write_string("         ", buf, &size);
        break;

    case lisid_Workorder:
        res = write(" %c%c %014ld", buf, &size,
                    data->Workorder.indStat,
                    data->Workorder.indUpdate,
                    data->Workorder.SampleId
                  );
        CHECK_WRITE_RES(res);
        CHECK_BUFSIZE;
        write_spaces(20, buf, &size);
        CHECK_BUFSIZE;
        if (data->Workorder.SpecCode == lis_speccode_None)
            write_spaces(4, buf, &size);
        else
        {
            res = write("%04d", buf, &size, data->Workorder.SpecCode);
            CHECK_WRITE_RES(res);
        }
        CHECK_BUFSIZE;
        res = write(" %014ld   % 30s %02d/%02d/%04d %c", buf, &size,
                    data->Workorder.Patient.Id,
                    data->Workorder.Patient.Name,
                    data->Workorder.Patient.BirthDate.Mounth,
                    data->Workorder.Patient.BirthDate.Day,
                    data->Workorder.Patient.BirthDate.Year,
                    data->Workorder.Patient.Sex
                  );
        CHECK_WRITE_RES(res);
        CHECK_BUFSIZE;
        res = write(" %02d/%02d/%02d %02d%02d", buf, &size,
                    data->Workorder.CollectionDate.Mounth,
                    data->Workorder.CollectionDate.Day,
                    data->Workorder.CollectionDate.Year % 100,
                    data->Workorder.CollectionTime.Hour,
                    data->Workorder.CollectionTime.Minutes
                  );
        CHECK_WRITE_RES(res);
        CHECK_BUFSIZE;
        res = write(" % 6s % 6s \r\n %03d", buf, &size,
                    data->Workorder.Location,
                    data->Workorder.Doctor,
                    data->Workorder.TestNumber[0]
                  );
        CHECK_WRITE_RES(res);
        CHECK_BUFSIZE;
        if(data->Workorder.PreviousTest.code != lis_prevtest_No)
        {
            CHECK_BUFSIZE;
            int x,y,z;
            if (data->Workorder.PreviousTest.code == lis_prevtest_ForDate)
            {
                x = data->Workorder.PreviousTest.DateTime.date.Mounth;
                y = data->Workorder.PreviousTest.DateTime.date.Day;
                z = data->Workorder.PreviousTest.DateTime.date.Year % 100;
            }
            else
            {
                x = data->Workorder.PreviousTest.DateTime.time.Hour;
                y = data->Workorder.PreviousTest.DateTime.time.Minutes;
                z = data->Workorder.PreviousTest.DateTime.time.Seconds;
            }
            res = write("%c%02d%02d%02d%05d", buf, &size,
                        data->Workorder.PreviousTest.code,
                        x,y,z,
                        data->Workorder.PreviousTest.result % 100000
                      );
            CHECK_WRITE_RES(res);
        }
        for (i = 1; i < data->Workorder.TestsCount; i++)
        {
            CHECK_BUFSIZE;
            res = write("%03d", buf, &size, data->Workorder.TestNumber[i]);
            CHECK_WRITE_RES(res);
        }
        break;

    case lisid_WorkorderValidation:
        res = write("       %c%c", buf, &size,
                     data->WorkorderValidation.Mode,
                     data->WorkorderValidation.Error
                  );
        CHECK_WRITE_RES(res);
        break;

    case lisid_Query:
        break;

    case lisid_NoWorkorder:
        break;

    case lisid_Result:
        res = write("%014ld", buf, &size,
                    data->Result.SampleId
                    );
        CHECK_WRITE_RES(res);
        CHECK_BUFSIZE;
        if (data->Result.RackAndPos.isValid)
        {
            res = write(" %03d-%02d", buf, &size,
                        data->Result.RackAndPos.Rack % 1000,
                        data->Result.RackAndPos.Position % 100
                        );
            CHECK_WRITE_RES(res);
        }
        else
            write_spaces(7, buf, &size);
        CHECK_BUFSIZE;
        write_spaces(6, buf, &size);
        CHECK_BUFSIZE;
        if (data->Result.SpecCode == lis_speccode_None)
            write_spaces(4, buf, &size);
        else
        {
            res = write("%04d% 13s", buf, &size,
                        data->Result.SpecCode,
                        data->Result.SpecName
                        );
            CHECK_WRITE_RES(res);
        }
        CHECK_BUFSIZE;
        res = write(" %02d/%02d/%02d %02d:%02d:%02d   \r\n", buf, &size,
                    data->Result.AspirationDate.Mounth,
                    data->Result.AspirationDate.Day,
                    data->Result.AspirationDate.Year % 100,
                    data->Result.AspirationTime.Hour,
                    data->Result.AspirationTime.Minutes,
                    data->Result.AspirationTime.Seconds
                    );
        CHECK_WRITE_RES(res);
#ifdef LIS_TESTRES_ISBUF
        CHECK((size + data->Result.TestResultsBufSize < bufsize), liserr_BufOverflow);
        write_string(data->Result.TestResultsBuf, buf, &size);
#else
        for (i = 0; i < data->Result.TestsCount; ++i)
        {
            CHECK_BUFSIZE;
            res = write("%03d%05d%s%03d|%s|", buf, &size,
                        data->Result.TestResults[i].Number,
                        data->Result.TestResults[i].Result,
                        data->Result.TestResults[i].Flag,
                        data->Result.TestResults[i].UserCode,
                        data->Result.TestResults[i].ReducedCodes
                        );
            CHECK_WRITE_RES(res);
        }
#endif
        break;

    case lisid_ResultValidation:
        write_spaces(16, buf, &size);
        CHECK_BUFSIZE;
        res = write(" %c", buf, &size,
                     data->ResultValidation.Error
                  );
        CHECK_WRITE_RES(res);
        break;
    }
    CHECK_BUFSIZE;
    write_string("\r\n", buf, &size);
    return size;
}
int lis_write_pdu(lis_ProtocolDataUnit_t *pdu, LIS_DATA* buf, LIS_SIZE bufsize)
{
    LIS_SIZE size = 0;
    int res = 0;
    CHECK_BUFSIZE;
    write_byte(LIS_STX, buf, &size);
    CHECK_BUFSIZE;
    write_byte(pdu->mt, buf, &size);
    CHECK_BUFSIZE;
    write_byte(lis_id_char(pdu->id), buf, &size);
    CHECK_BUFSIZE;
    write_byte(' ', buf, &size);

    res = lis_write_data(pdu->id, &pdu->data, buf+size, bufsize-size);
    if (res < 0) return res;
    size += res;

    CHECK_BUFSIZE;
    write_byte(calc_lrc(buf+1,size-1), buf, &size);
    CHECK_BUFSIZE;
    write_byte(LIS_ETX, buf, &size);

    return size;
}
#undef CHECK_WRITE_RES
#undef CHECK_BUFSIZE

//---------------------------

static inline LIS_DATA read_byte (LIS_DATA* buf, LIS_SIZE* size)
{
    return buf[(*size)++];
}

static int read(const char* format, LIS_DATA* buf, LIS_SIZE size, ...)
{
    va_list args;
    va_start(args, size);
    int res = vsscanf((char*)buf+size, format, args);
    va_end(args);
    return res;
}

static void read_string(char* str, LIS_SIZE n, LIS_DATA* buf, LIS_SIZE* size)
{
    int tmp;
    for (tmp = 0; tmp < n; ++tmp)
        str[tmp] = read_byte(buf, size);
    str[n] = 0;
}

int check_spaces(LIS_SIZE count, LIS_DATA* buf, LIS_SIZE* size)
{
    int x;
    for (x = 0; x < count; x++)
        if (read_byte(buf, size) != ' ')
            return 0;
    return 1;
}

#define CHECK_BUFSIZE CHECK((size < bufsize), liserr_BufOverflow)
#define CHECK_READ_RES(x, n, y) do{CHECK(((x == n)&&(y > 0)), liserr_ReadFailure); size += y; y = 0;} while(0)
#define CHECK_SPACES_RES(x) CHECK((x == 1), liserr_InvalidSpace)
int lis_read_data(LIS_DATA *buf, LIS_SIZE bufsize, lis_IdCode_t id, lis_Data_t* data)
{
    LIS_SIZE size = 0;
    int res = 0, count = 0, tmp;
    char c[101];

    CHECK_BUFSIZE;
    switch (id)
    {
    default:
        LIS_ASSERT(0);
        return liserr_General;

    case lisid_Initialization:
        break;

    case lisid_TokenTransfer:
        res = check_spaces(9, buf, &size);
        CHECK_SPACES_RES(res);
        break;

    case lisid_Workorder:{
        res = read(" %c%c %ld%n", buf, size,
                    &c[0], &c[1],
                    &data->Workorder.SampleId,
                    &count
                  );
        CHECK_READ_RES(res, 3, count);
        data->Workorder.indStat = c[0];
        data->Workorder.indUpdate = c[1];

        CHECK_BUFSIZE;
        res = check_spaces(20, buf, &size);
        CHECK_SPACES_RES(res);

        CHECK_BUFSIZE;
        if (check_spaces(4, buf, &size))
            data->Workorder.SpecCode = lis_speccode_None;
        else
        {
            res = read("%d%n", buf, size,
                        &data->Workorder.SpecCode,
                        &count
                     );
            CHECK_READ_RES(res, 1, count);
        }

        CHECK_BUFSIZE;
        res = read(" %ld%n", buf, size,
                    &data->Workorder.Patient.Id,
                    &count
                   );
        CHECK_READ_RES(res, 1, count);

        CHECK_BUFSIZE;
        res = check_spaces(3, buf, &size);
        CHECK_SPACES_RES(res);

        CHECK_BUFSIZE;
        for (tmp = 0; tmp < 30; ++tmp)
            data->Workorder.Patient.Name[tmp] = read_byte(buf, &size);

        CHECK_BUFSIZE;
        res = read(" %d/%d/%d %c%n", buf, size,
                    &data->Workorder.Patient.BirthDate.Mounth,
                    &data->Workorder.Patient.BirthDate.Day,
                    &data->Workorder.Patient.BirthDate.Year,
                    &c[0],
                    &count
                  );
        CHECK_READ_RES(res, 4, count);
        data->Workorder.Patient.Sex = c[0];

        CHECK_BUFSIZE;
        res = read(" %d/%d/%d %d%n", buf, size,
                    &data->Workorder.CollectionDate.Mounth,
                    &data->Workorder.CollectionDate.Day,
                    &data->Workorder.CollectionDate.Year,
                    &tmp,
                    &count
                  );
        CHECK_READ_RES(res, 4, count);
        data->Workorder.CollectionTime.Hour = tmp/100;
        data->Workorder.CollectionTime.Minutes = tmp%100;
        data->Workorder.CollectionTime.Seconds = 0;

        CHECK_BUFSIZE;
        res = check_spaces(1, buf, &size);
        CHECK_SPACES_RES(res);

        CHECK_BUFSIZE;
        read_string(data->Workorder.Location, 6, buf, &size);

        CHECK_BUFSIZE;
        res = check_spaces(1, buf, &size);
        CHECK_SPACES_RES(res);

        CHECK_BUFSIZE;
        read_string(data->Workorder.Doctor, 6, buf, &size);

        CHECK_BUFSIZE;
        res = read(" \r\n %d%n", buf, size,
                    &data->Workorder.TestNumber[0],
                    &count
                  );
        CHECK_READ_RES(res, 1, count);
        data->Workorder.TestsCount = 1;

        CHECK_BUFSIZE;
        res = buf[size];
        if ((res == lis_prevtest_ForTime)||(res == lis_prevtest_ForDate))
        {
            data->Workorder.PreviousTest.code = read_byte(buf, &size);
            CHECK_BUFSIZE;

            int x,y,z;
            c[2] = 0;
            memcpy(c, buf+size, 2);
            x = atoi(c);
            size += 2;
            memcpy(c, buf+size, 2);
            y = atoi(c);
            size += 2;
            memcpy(c, buf+size, 2);
            z = atoi(c);
            size += 2;

            c[5] = 0;
            memcpy(c, buf+size, 5);
            data->Workorder.PreviousTest.result = atoi(c);
            size += 5;

            if (data->Workorder.PreviousTest.code == lis_prevtest_ForDate)
            {
                data->Workorder.PreviousTest.DateTime.date.Mounth = x;
                data->Workorder.PreviousTest.DateTime.date.Day = y;
                data->Workorder.PreviousTest.DateTime.date.Year = z;
            }
            else
            {
                data->Workorder.PreviousTest.DateTime.time.Hour = x;
                data->Workorder.PreviousTest.DateTime.time.Minutes = y;
                data->Workorder.PreviousTest.DateTime.time.Seconds = z;
            }
        }
        c[3] = 0;
        while(buf[size] != '\r')
        {
            CHECK_BUFSIZE;
            memcpy(c, buf+size, 3);
            data->Workorder.TestNumber[data->Workorder.TestsCount++] = atoi(c);
            size += 3;
        }
        break;}

    case lisid_WorkorderValidation:
        res = check_spaces(7, buf, &size);
        CHECK((res == 1), liserr_InvalidSpace);

        CHECK_BUFSIZE;
        res = read("%c%c%n", buf, size,
                     &c[0],
                     &c[1],
                     &count
                   );
        CHECK_READ_RES(res, 2, count);
        data->WorkorderValidation.Mode = c[0];
        data->WorkorderValidation.Error = c[1];
        break;

    case lisid_Query:
        break;

    case lisid_NoWorkorder:
        break;

    case lisid_Result:
        res = read("%ld%n", buf, size,
                    &data->Result.SampleId,
                    &count
                   );
        CHECK_READ_RES(res, 1, count);

        if (!check_spaces(7, buf, &size))
        {
            data->Result.RackAndPos.isValid = 1;
            res = read(" %d-%d%n", buf, size,
                        &data->Result.RackAndPos.Rack,
                        &data->Result.RackAndPos.Position,
                        &count
                        );
            CHECK_READ_RES(res, 2, count);
        }
        else
            data->Result.RackAndPos.isValid = 0;

        CHECK_BUFSIZE;
        res = check_spaces(6, buf, &size);
        CHECK((res == 1), liserr_InvalidSpace);

        CHECK_BUFSIZE;
        if (check_spaces(4, buf, &size))
            data->Result.SpecCode = lis_speccode_None;
        else
        {
            res = read("%d%n", buf, size,
                        &data->Result.SpecCode,
                        &count
                     );
            CHECK_READ_RES(res, 1, count);

            read_string(data->Result.SpecName, 13, buf, &size);
        }

        CHECK_BUFSIZE;
        res = read(" %d/%d/%d %d:%d:%d   \r\n%n", buf, size,
                    &data->Result.AspirationDate.Mounth,
                    &data->Result.AspirationDate.Day,
                    &data->Result.AspirationDate.Year,
                    &data->Result.AspirationTime.Hour,
                    &data->Result.AspirationTime.Minutes,
                    &data->Result.AspirationTime.Seconds,
                    &count
                    );
        CHECK_READ_RES(res, 6, count);

        int i = 0;
        while(buf[size] != '\r')
        {
            CHECK_BUFSIZE;
#ifdef LIS_TESTRES_ISBUF
            data->Result.TestResultsBuf[i] = read_byte(buf, &size);
#else
            read_string(c, 3, buf, &size);
            data->Result.TestResults[i].Number = atoi(c);

            read_string(c, 5, buf, &size);
            data->Result.TestResults[i].Result = atoi(c);

            read_string(data->Result.TestResults[i].Flag, 1, buf, &size);

            res = read("%d|%n", buf, size,
                       &data->Result.TestResults[i].UserCode,
                       &count
                       );
            CHECK_READ_RES(res, 1, count);

            tmp = 0;
            while(buf[size] != '|')
            {
                CHECK((tmp < LIS_TESTRES_REDUCEDCODES_BUFSIZE), liserr_BufOverflow);
                data->Result.TestResults[i].ReducedCodes[tmp++] = read_byte(buf, &size);
            }
            data->Result.TestResults[i].ReducedCodes[tmp] = 0;
            ++size; // учитываем последний символ '|'
#endif
            ++i;
        }
#ifdef LIS_TESTRES_ISBUF
        data->Result.TestResultsBuf[i] = 0;
        data->Result.TestResultsBufSize = i;
#else
        data->Result.TestsCount = i;
#endif
        break;

    case lisid_ResultValidation:
        res = check_spaces(16, buf, &size);
        CHECK((res == 1), liserr_InvalidSpace);

        CHECK_BUFSIZE;
        res = read(" %c%n", buf, size,
                     &c[0],
                     &count
                   );
        CHECK_READ_RES(res, 1, count);
        data->ResultValidation.Error = c[0];
        break;
    }

    CHECK_BUFSIZE;
    res = read_byte(buf, &size);
    CHECK((res == '\r'), liserr_InvalidCR);

    CHECK_BUFSIZE;
    res = read_byte(buf, &size);
    CHECK((res == '\n'), liserr_InvalidLF);

    return size;
}
int lis_read_pdu(LIS_DATA *buf, LIS_SIZE bufsize, lis_ProtocolDataUnit_t *pdu)
{
    LIS_SIZE size = 0;
    int res = 0;

    CHECK_BUFSIZE;
    res = read_byte(buf, &size);
    CHECK((res == LIS_STX), liserr_InvalidSTX);

    CHECK_BUFSIZE;
    pdu->mt = read_byte(buf, &size);
    CHECK_BUFSIZE;
    pdu->id = lis_char_id(read_byte(buf, &size));
    CHECK_BUFSIZE;
    res = read_byte(buf, &size);
    CHECK((res == ' '), liserr_InvalidSpace);

    res = lis_read_data(buf+size, bufsize-size, pdu->id, &pdu->data);
    if (res < 0) return res;
    size += res;

    CHECK_BUFSIZE;
    int lrc = calc_lrc(buf+1,size-1);
    res = read_byte(buf, &size);
    CHECK((lrc == res), liserr_InvalidLRC);

    CHECK_BUFSIZE;
    res = read_byte(buf, &size);
    CHECK((res == LIS_ETX), liserr_InvalidETX);

    return size;
}
#undef CHECK_READ_RES
#undef CHECK_BUFSIZE

const char* lis_error_text(lis_ErrorCode_t err)
{
    switch (err)
    {
    default: return "unknown error";
    case liserr_BufOverflow: return "buffer overflow";
    case liserr_WriteFailure: return "write failure";
    case liserr_ReadFailure: return "read failure";
    case liserr_InvalidSTX: return "invalid STX";
    case liserr_InvalidMT: return "invalid MT";
    case liserr_InvalidLRC: return "invalid LRC";
    case liserr_InvalidETX: return "invalid ETX";
    case liserr_InvalidID: return "invalid ID";
    case liserr_InvalidSpace: return "invalid space after ID";
    case liserr_InvalidCR: return "invalid CR";
    case liserr_InvalidLF: return "invalid LF";
    }
}

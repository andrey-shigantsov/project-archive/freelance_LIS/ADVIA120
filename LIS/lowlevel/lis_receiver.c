/*!
 * \file lis_receiver.c
 * \author Андрей Шиганцов
 * \brief Исходные коды приёмника сообщений по протоколу LIS
 */

#include "lis_receiver.h"
#include "lis_codetable.h"

void init_lisReceiver(lisReceiver_t *This, lisReceiverCfg_t *cfg)
{
    This->Parent = cfg->Parent;
    This->mt_ready = cfg->mt_ready;
    This->pdu_ready = cfg->pdu_ready;

    reset_lisReceiver(This);
}
void reset_lisReceiver(lisReceiver_t* This)
{
    This->State = lisrstate_Waiting;
    This->Counter = 0;
}

int lis_receiver_read_byte(lisReceiver_t *This, LIS_DATA byte)
{
    switch(This->State)
    {
    case lisrstate_Waiting:
        if (byte != LIS_STX)
        {
            This->mt_ready(This->Parent, byte);
            break;
        }
        This->State = lisrstate_Receiving;
        This->Counter = 0;
    case lisrstate_Receiving:
        if (This->Counter >= LIS_MAX_BUFSIZE)
            return liserr_BufOverflow;

        This->buf[This->Counter++] = byte;
        if (byte == LIS_ETX)
        {
            This->State = lisrstate_Waiting;
            This->pdu_ready(This->Parent, This->buf, This->Counter);
        }
        break;
    }
    return 0;
}

int lis_receiver_read_buf(lisReceiver_t *This, LIS_DATA* buf, LIS_SIZE size)
{
    int i;
    for (i = 0; i < size; i++)
    {
        int err = lis_receiver_read_byte(This, buf[i]);
        if (err) return err;
    }
    return 0;
}

/*!
 * \file lis.h
 * \author Андрей Шиганцов
 * \brief Заголовочный файл реализации кодирования по протоколу LIS
 */

#ifndef __LIS_H__
#define __LIS_H__

#include "lis_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

#define LIS_MT_MIN 0x30u
#define LIS_MT_MAX 0x5au

#define LIS_PATIENT_NAME_BUFSIZE (30+1)
#define LIS_LOCATION_BUFSIZE (6+1)
#define LIS_DOCTOR_BUFSIZE (6+1)

#define LIS_TESTS_MAXCOUNT 110

#define LIS_SPECNAME_BUFSIZE (13+1)

#ifdef LIS_TESTRES_ISBUF
#define LIS_TESTRES_BUFSIZE (LIS_TESTS_MAXCOUNT*100+1)
#else
#define LIS_TESTRES_FLAG_BUFSIZE (2+1)
#define LIS_TESTRES_REDUCEDCODES_BUFSIZE ((15*3)+1)
#endif

/*! Коды сообщений */
typedef enum
{
    lisid_Initialization,
    lisid_TokenTransfer,
    lisid_Workorder,
    lisid_WorkorderValidation,
    lisid_Result,
    lisid_Query,
    lisid_NoWorkorder,
    lisid_ResultValidation,
//---------------------------
    invalid_lisid = -1
} lis_IdCode_t;

/*! STAT INDICATOR */
typedef enum
{
    lis_stat_Yes = 'U',
    lis_stat_No = ' '
} lis_StatIndicator_t;

/*! Update Indicator */
typedef enum
{
    lis_update_Yes = 'A',
    lis_update_No = ' '
} lis_UpdateIndicator_t;

typedef struct
{
    int Day, Mounth;
    int Year;
} lis_Date_t;
typedef struct
{
    int Hour, Minutes, Seconds;
} lis_Time_t;

typedef enum
{
    lis_sex_Male = 'M',
    lis_sex_Female = 'F',
    lis_sex_Unknown = ' '
} lis_Sex_t;

typedef struct
{
    long Id; /*!< Patient Identification Number (hospital number) */
    char Name[LIS_PATIENT_NAME_BUFSIZE];
    lis_Date_t BirthDate;
    lis_Sex_t Sex;
} lis_Patient_t;

typedef enum
{
    lis_prevtest_No = -1,
    lis_prevtest_ForDate = 'D',
    lis_prevtest_ForTime = 'T'
} lis_PreviousTestCode_t;

/*! Species Code */
typedef enum
{
    lis_speccode_None = -1,
    lis_speccode_Human = 0,
    lis_speccode_Rat = 100,
    lis_speccode_RatLE = 200,
    lis_speccode_RatSD = 300,
    lis_speccode_RatWistar = 400,
    lis_speccode_Mouse = 500,
    lis_speccode_MouseBalb = 600,
    lis_speccode_MouseCD1 = 700,
    lis_speccode_MouseC57BL = 800,
    lis_speccode_CynomolgusMonkey = 900,
    lis_speccode_RhesusMonkey = 1000,
    lis_speccode_Rabbit = 1100,
    lis_speccode_RabbitNZW = 1200,
    lis_speccode_GuineaPig = 1300,
    lis_speccode_Dog = 1400,
    lis_speccode_Cat = 1500,
    lis_speccode_Hourse = 1600,
    lis_speccode_Cattle = 1700,
    lis_speccode_Goat = 1800,
    lis_speccode_Pig = 1900,
    lis_speccode_Sheep = 2000
} lis_SpecCode_t;

/*! Структура данных Workorder сообщения */
typedef struct
{
    lis_StatIndicator_t indStat;
    lis_UpdateIndicator_t indUpdate;
    long SampleId; /*!< Sample Identification Number */
    lis_SpecCode_t SpecCode;
    lis_Patient_t Patient;
    lis_Date_t CollectionDate;
    lis_Time_t CollectionTime;
    char Location[LIS_LOCATION_BUFSIZE];
    char Doctor[LIS_DOCTOR_BUFSIZE];
    short TestNumber[LIS_TESTS_MAXCOUNT];
    int TestsCount;
    struct
    {
        lis_PreviousTestCode_t code;
        union
        {
            lis_Date_t date;
            lis_Time_t time;
        } DateTime;
        int result;
    } PreviousTest;
} lis_WorkorderData_t;

typedef enum
{
    lis_wrkvalmode_D = ' ',
    lis_wrkvalmode_Q = '1'
} lis_WorkorderValidationMode_t;
typedef enum
{
    lis_wrkvalerr_No = '0',
    lis_wrkvalerr_Yes = '4'
} lis_WorkorderValidationError_t;

/*! Структура данных Workorder Validation сообщения */
typedef struct
{
    lis_WorkorderValidationMode_t Mode;
    lis_WorkorderValidationError_t Error;
} lis_WorkorderValidationData_t;

#ifndef LIS_TESTRES_ISBUF
typedef struct
{
    int Number;
    int Result;
    char Flag[LIS_TESTRES_FLAG_BUFSIZE];
    int UserCode;
    char ReducedCodes[LIS_TESTRES_REDUCEDCODES_BUFSIZE];
} lis_TestResult_t;
#endif
/*! Структура данных Result сообщения */
typedef struct
{
    long SampleId; /*!< Sample Identification Number */
    struct
    {
        char isValid;
        int Rack;
        int Position;
    } RackAndPos;
    lis_SpecCode_t SpecCode;
    char SpecName[LIS_SPECNAME_BUFSIZE];
    lis_Date_t AspirationDate;
    lis_Time_t AspirationTime;
#ifdef LIS_TESTRES_ISBUF
    char TestResultsBuf[LIS_TESTRES_BUFSIZE];
    LIS_SIZE TestResultsBufSize;
#else
    lis_TestResult_t TestResults[LIS_TESTS_MAXCOUNT];
    LIS_SIZE TestsCount;
#endif
} lis_ResultData_t;

typedef enum
{
    lis_resvalerr_No = '0',
    lis_resvalerr_FileFull = '1',
    lis_resvalerr_Stop = '2'
} lis_ResultValidationError_t;
/*! Структура данных ResultValidation сообщения */
typedef struct
{
    lis_ResultValidationError_t Error;
} lis_ResultValidationData_t;

/*! Набор всех возможных данных */
typedef union
{
    lis_WorkorderData_t Workorder;
    lis_WorkorderValidationData_t WorkorderValidation;
    lis_ResultData_t Result;
    lis_ResultValidationData_t ResultValidation;
} lis_Data_t;

/*! Общая структура данных сообщения */
typedef struct
{
    LIS_DATA mt;
    lis_IdCode_t id;
    lis_Data_t data;
} lis_ProtocolDataUnit_t;

/*! Коды ошибок */
typedef enum
{
    liserr_General = -1,
    liserr_BufOverflow = -2,

    liserr_WriteFailure = -0x11,

    liserr_ReadFailure = -0x20,
    liserr_InvalidSTX = -0x21,
    liserr_InvalidMT = -0x22,
    liserr_InvalidLRC = -0x23,
    liserr_InvalidETX = -0x24,
    liserr_InvalidID = -0x25,
    liserr_InvalidSpace = -0x26,
    liserr_InvalidCR = -0x27,
    liserr_InvalidLF = -0x28
} lis_ErrorCode_t;

/*! Заисать данные сообщения в буфер */
/*! \return количество записанных байт, либо отрицательный код ошибки (lis_ErrorCode_t) */
int lis_write_data(lis_IdCode_t id, lis_Data_t* data, LIS_DATA* buf, LIS_SIZE bufsize);
/*! Заисать сообщение в буфер */
/*! \return количество записанных байт, либо отрицательный код ошибки (lis_ErrorCode_t) */
int lis_write_pdu
(
    lis_ProtocolDataUnit_t* pdu /*!< [in] сообщение */,
    LIS_DATA* buf /*!< [out] буфер под сообщение */,
    LIS_SIZE bufsize /*!< [in] размер буфера под соощение */
);

/*! Считать данные сообщения из буфера */
/*! \return число прочитанных байт в случае успеха, либо отрицательный код ошибки (lis_ErrorCode_t) */
int lis_read_data(LIS_DATA *buf, LIS_SIZE bufsize, lis_IdCode_t id, lis_Data_t* data);
/*! Считать сообщение из буфера */
/*! \return число прочитанных байт в случае успеха, либо отрицательный код ошибки (lis_ErrorCode_t) */
int lis_read_pdu
(
    LIS_DATA* buf /*!< [in] буфер сообщения */,
    LIS_SIZE bufsize /*!< [in] размер буфера сообщения */,
    lis_ProtocolDataUnit_t* pdu /*!< [out] сообщение */
);

/*! Текст сообщения об ошибке */
const char* lis_error_text(lis_ErrorCode_t err);

/*! Получить символ соответствующий id */
char lis_id_char(lis_IdCode_t id);
/*! Получить соответствующий id*/
lis_IdCode_t lis_char_id(char data);

#ifdef __cplusplus
}
#endif

#endif//__LIS_H__

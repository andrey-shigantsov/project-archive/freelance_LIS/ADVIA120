/*!
 * \file TLisInterfaceCom.h
 * \author Андрей Шиганцов
 * \brief Заголовочный файл класса COM интерфейса для протокола LIS
 */

#ifndef TLISINTERFACECOM_H
#define TLISINTERFACECOM_H

#include "TLisInterface.h"
#include <QSerialPort>

/*!< Класс COM интерфейса для протокола LIS */
class TLisInterfaceCom : public TLisInterface
{
    Q_OBJECT
public:
    explicit TLisInterfaceCom(QObject *parent = 0);

    /*! Открыть соединение с COM портом */
    /*! \return флаг успешного выполнения, в случае ошибки посылается сигнал TLisInterface::error(QString,QString) */
    bool open_conection(QString portName /*!< [in] Имя порта */,
              qint32 baudrate /*!< [in] Символьная скорость */,
              QSerialPort::DataBits dataBits /*!< [in] Количество бит данных */,
              QSerialPort::Parity parity /*!< [in] Проверка чётности */,
              QSerialPort::FlowControl flowCtrl /*! [in] Управление потоком */);
    /*! Закрыть соединение с COM портом */
    void close_connection();

private:
    /*! Последовательное устройство */
    QSerialPort dev;

protected:
    /*! Отравить данные */
    bool send(LIS_DATA data);
    /*! Отправить буфер */
    bool send(LIS_DATA* buf, LIS_SIZE size);

private slots:
    /*! Обработчик приходящих данных, срабатывает по сигналу QSerialPort::readyRead() */
    void read();
};

#endif // TLISINTERFACECOM_H

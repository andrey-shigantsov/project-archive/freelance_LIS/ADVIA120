#ifndef TLISFILE_H
#define TLISFILE_H

#include <QObject>
#include "lowlevel/lis.h"

class TLisFile : public QObject
{
    Q_OBJECT
public:
    explicit TLisFile(QObject *parent = 0);

    inline QString FilePath(){return fPath;}
    void setFilePath(QString filepath);

    void write(lis_IdCode_t id, lis_Data_t *data);
    void write(QString FullFileName, lis_IdCode_t id, lis_Data_t *data);

    bool read(QString FullFileName, lis_IdCode_t id, lis_Data_t *data);

private:
    QString fPath;

signals:
    /*! Сообщение */
    void message(QString msg);
    /*! Предупреждение */
    void warning(QString src, QString msg);
    /*! Ошибка */
    void error(QString src, QString msg);

public slots:
};

#endif // TLISFILE_H

#include "TLisInterfaceCom.h"

TLisInterfaceCom::TLisInterfaceCom(QObject *parent) :
    TLisInterface(QString("COM"), parent)

{
    connect(&dev, SIGNAL(readyRead()), this, SLOT(read()));
}

bool TLisInterfaceCom::open_conection(QString portName,
                       qint32 baudrate,
                       QSerialPort::DataBits dataBits,
                       QSerialPort::Parity parity,
                       QSerialPort::FlowControl flowCtrl)
{
    dev.setPortName(portName);
    if (!dev.open(QIODevice::ReadWrite))
    {
        send_error(tr("Подключение"), QString("%1: %2").arg(portName).arg(dev.errorString()));
        return false;
    }

    dev.setBaudRate(baudrate);
    dev.setDataBits(dataBits);
    dev.setParity(parity);
    dev.setFlowControl(flowCtrl);

    setConnectStatus(Connected);

    return true;
}

void TLisInterfaceCom::close_connection()
{
    setConnectStatus(Disconnected);
    resetState();
    dev.close();
}

bool TLisInterfaceCom::send(LIS_DATA data)
{
    return send(&data, 1);
}

bool TLisInterfaceCom::send(LIS_DATA *buf, LIS_SIZE size)
{
    qint64 n = dev.write((const char*)buf,size);
    if ((n < 0)||(n != size))
    {
        send_error("Отправка", tr("ошибка записи: %1").arg(dev.errorString()));
        return false;
    }
    return true;
}

void TLisInterfaceCom::read()
{
    QByteArray buf = dev.readAll();
    read_buf((LIS_DATA*)buf.data(), buf.size());
}

#include "TLisInterface.h"
#include "lowlevel/lis_codetable.h"

TLisInterface::TLisInterface(QString Name, QObject *parent) : QObject(parent)
{
    this->Name = Name;

    lisReceiverCfg_t cfgR;
    cfgR.Parent = this;
    cfgR.pdu_ready = receiver_pdu_ready;
    cfgR.mt_ready = receiver_mt_ready;
    init_lisReceiver(&receiver, &cfgR);

    cStatus = Disconnected;
    resetState();

    connect(&WatchdogTimer, SIGNAL(timeout()), this, SLOT(watchdog_timeout_handler()));

    maxNackCount = 2;
    WatchdogTimeout = 40000;
}

void TLisInterface::reset()
{
    resetOperationStatus();
    resetState();
}

void TLisInterface::resetState()
{
    currentMT = LIS_MT_MAX;
    oStatus = Ready;
    nackCounter = 0;
}

void TLisInterface::send_init()
{
    pdu.id = lisid_Initialization;
    pdu.mt = getMT();
    send_pdu(&pdu);
}

void TLisInterface::send_workorder(lis_WorkorderData_t* data)
{
    pdu.id = lisid_Workorder;
    pdu.mt = getMT();
    pdu.data.Workorder = *data;
    send_pdu(&pdu);
}

void TLisInterface::send_token()
{
    pdu.id = lisid_TokenTransfer;
    pdu.mt = getMT();
    send_pdu(&pdu);
}

void TLisInterface::send_resultvalidation(lis_ResultValidationData_t* data)
{
    pdu.id = lisid_ResultValidation;
    pdu.mt = getMT();
    pdu.data.ResultValidation = *data;
    send_pdu(&pdu);
}

bool TLisInterface::send_pdu(lis_ProtocolDataUnit_t *pdu, bool isCheckOperation)
{
    if (isCheckOperation)
        if (!check_operation_status()) return false;

    LIS_DATA buf[LIS_MAX_BUFSIZE];
    LIS_SIZE size = lis_write_pdu(pdu, buf, LIS_MAX_BUFSIZE);
    if (size < 0)
    {
        send_error(tr("Кодер"), errorString(size));
        return false;
    }
    emit pdu_sent(*pdu);

    setOperationStatus(Waiting);
    if (&this->pdu != pdu) this->pdu = *pdu;

    WatchdogTimer.start(WatchdogTimeout);
    return send(buf,size);
}

void TLisInterface::nack_proc()
{
    nackCounter++;
    if (nackCounter >= maxNackCount)
    {
        setOperationStatus(TLisInterface::Error);
        emit OperationResult_ready(TLisInterface::nack_MaxCountExceeded);
    }
}

void TLisInterface::receiver_pdu_ready(void *This, LIS_DATA *buf, LIS_SIZE size)
{
    TLisInterface* t = (TLisInterface*)This;

    lis_ProtocolDataUnit_t pdu;
    int res = lis_read_pdu(buf, size, &pdu);
    if (res >= 0)
    {
        t->send(pdu.mt);
        t->nack_reset();
        t->currentMT = pdu.mt;
        emit t->pdu_received(pdu);
        return;
    }
    switch (res)
    {
    default:
        t->send_error(tr("Декодер"), errorString(res));
    case liserr_InvalidLRC:
        t->send(LIS_NACK);
        t->nack_proc();
        break;
    }

}

void TLisInterface::receiver_mt_ready(void *This, LIS_DATA mt)
{
    TLisInterface* t = (TLisInterface*)This;
    if (t->getOperationStatus() != Waiting)
    {
        t->send_error(tr("Интерфейс"), tr("неожиданно принят MT = %1)").arg(mt));
        return;
    }
    t->WatchdogTimer.stop();
    if (mt == t->currentMT)
    {
        t->setOperationStatus(Ready);
        t->nack_reset();
        emit t->OperationResult_ready(TLisInterface::Success);
    }
    else if ((mt == LIS_NACK)||((mt >= LIS_MT_MIN)&&(mt <= LIS_MT_MAX)))
    {
        t->send_pdu(&t->pdu, 0);
        t->nack_proc();
    }
    else
        t->send_error(tr("Интерфейс"), tr("принят некорректный MT = %1)").arg(mt));
}

void TLisInterface::watchdog_timeout_handler()
{
    WatchdogTimer.stop();
    send_error(tr("Интерфейс"), tr("устройство не отвечает"));
}

void TLisInterface::send_error(QString src, QString msg)
{
    QString fullSrc = Name + ": " + src;
    emit error(fullSrc, msg);
}

void TLisInterface::setConnectStatus(TLisInterface::ConnectStatus status)
{
    cStatus = status;

    emit ConnectStatus_changed(status);
}

void TLisInterface::setOperationStatus(TLisInterface::OperationStatus status)
{
    oStatus = status;

    emit OperationStatus_changed(status);
}

bool TLisInterface::check_operation_status()
{
    if (oStatus == Waiting)
    {
        send_error(tr("Интерфейс"), tr("предыдущая операция ещё не завершена"));
        return false;
    }
    return true;
}

LIS_DATA TLisInterface::getMT()
{
    ++currentMT;
    if (currentMT > LIS_MT_MAX) currentMT = LIS_MT_MIN;
    return currentMT;
}

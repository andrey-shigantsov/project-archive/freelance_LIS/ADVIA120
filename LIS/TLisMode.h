/*!
 * \file TLisMode.h
 * \author Андрей Шиганцов
 * \brief Заголовочный файл класса реализации Downloading workorder mode протокола LIS
 */

#ifndef TLISDOWNLOADINGMODE_H
#define TLISDOWNLOADINGMODE_H

#include <QObject>
#include <QList>
#include "TLisInterface.h"

/*! Класс обеспечивающий работу режимов протокола LIS */
class TLisMode : public QObject
{
    Q_OBJECT
public:
    enum Type {DownloadingMode, QueryMode, FullQueryMode};
    enum Phase{Initialisation, WorkorderTransmission, TokenTransfer, ResultsTransmission, ResultValidationTransfer, Idle = -1};

    explicit TLisMode(TLisInterface* interface, Type mode = DownloadingMode, QObject *parent = 0);
    ~TLisMode();

    /*! Имя режима*/
    static QString TypeName(Type type);

    /*! Установить режим */
    inline void setType(Type mode){Mode = mode;}
    /*! Добавить задание на передачу */
    void appendWorkorder(QString filename, lis_WorkorderData_t data) {Workorders.append(data); WorkordersId.append(filename);}
    /*! Запуск */
    void start();
    /*! Остановка */
    void stop();
    /*! Сброс */
    void reset();
    /*! Состояние режима */
    bool isStarted(){return !isStop;}

signals:
    /*! Важное сообщение готово */
    void pdu_ready(lis_ProtocolDataUnit_t pdu);
    /*! Результат отправки задания */
    void workorder_transmitted(QString filename, bool isValid);

    /*! Сообщение */
    void message(QString msg);
    /*! Предупреждение */
    void warning(QString src, QString msg);
    /*! Ошибка */
    void error(QString src, QString msg);

public slots:

protected:
    /*! Инициализация устройства */
    void init_device();
    /*! Реинициализация устройства */
    void reinit_device();
    /*! Передать задание */
    /*! \return Количество оставшихся заданий */
    int transmit_workorder();
    /*! Передать токен */
    void transmit_token();
    /*! Передать подтверждение результата */
    void transmit_resultvalidation();

    /*! Отправить предупреждение */
    void send_warning(QString src, QString msg);
    /*! Отправить ошибку */
    void send_error(QString src, QString msg);

private:
    TLisInterface* ifX;
    Type Mode;
    Phase phase;
    QList<lis_WorkorderData_t> Workorders;
    QList<QString> WorkordersId;
    QString currentWorkorderId;
    bool isStop;

    void set_phase(Phase phase, QString text);
    void success_handler_d();
    void pdu_handler_d(lis_ProtocolDataUnit_t* pdu);
private slots:
    /*! Обработчик результата операции */
    void OperationResult_handler(TLisInterface::OperationResult res /*!< [in] код результатата */);
    /*! Обработчик принятого сообщения */
    void pdu_handler(lis_ProtocolDataUnit_t pdu);
};

#endif // TLISDOWNLOADINGMODE_H

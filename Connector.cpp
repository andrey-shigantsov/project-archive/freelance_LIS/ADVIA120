#include "Connector.h"
#include "ui_Connector.h"

#include <QSerialPortInfo>

Connector::Connector(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Connector)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Tool);

    refresh_ports();

    interface = 0;
}

Connector::~Connector()
{
    delete ui;
}

void Connector::refresh_ports()
{
    QString old = ui->Port->currentText();
    ui->Port->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        ui->Port->addItem(info.portName());
    ui->Port->setEditText(old);
}

QString Connector::Port()
{
    return ui->Port->currentText();
}

quint32 Connector::BaudRate()
{
    return ui->BaudRate->currentText().toUInt();
}

QSerialPort::DataBits Connector::DataBits()
{
    return (QSerialPort::DataBits)ui->DataBits->currentText().toInt();
}

QSerialPort::Parity Connector::Parity()
{
    QString x = ui->Parity->currentText();
    if (x == "NONE")
        return QSerialPort::NoParity;
    if (x == "Even")
        return QSerialPort::EvenParity;
    if (x == "Odd")
        return QSerialPort::OddParity;
    if (x == "Space")
        return QSerialPort::SpaceParity;
    if (x == "Mark")
        return QSerialPort::MarkParity;
    return QSerialPort::UnknownParity;
}

QSerialPort::StopBits Connector::StopBits()
{
    return (QSerialPort::StopBits)ui->StopBits->currentText().toInt();
}

QSerialPort::FlowControl Connector::FlowControl()
{
    return QSerialPort::NoFlowControl;
}

void Connector::on_Connect_clicked()
{
    if (!interface)
        emit error("Подключение", tr("не иницициализирован интерфейс"));

    if (interface->open_conection(Port(), BaudRate(), DataBits(), Parity(), FlowControl()))
        close();
}

/*!
 * \file lis_cfg.h
 * \author Андрей Шиганцов
 * \breif Файл настроек реализации кодирования по протоколу LIS
 */

/*! Основной тип данных */
typedef unsigned char LIS_DATA;
/*! Тип данных для размера */
typedef int LIS_SIZE;

/*! Размер буфера под сообщение */
#define LIS_MAX_BUFSIZE 512

/*! Все результаты тестов считываются одной строкой */
#define LIS_TESTRES_ISBUF

#include "c_assert.h"
#define LIS_ASSERT(cond) if (!(cond)) c_assert(#cond,__FILE__,__LINE__);

#ifndef C_ASSERT_H
#define C_ASSERT_H

#ifdef __cplusplus
extern "C" {
#endif

void c_assert(const char *assertion, const char *file, int line);
#define C_ASSERT(cond) if (!(cond)) c_assert(#cond,__FILE__,__LINE__)

#ifdef __cplusplus
}
#endif

#endif // C_ASSERT_H


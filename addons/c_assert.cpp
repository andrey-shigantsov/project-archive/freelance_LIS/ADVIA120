#include <qglobal.h>
#include <QDebug>

extern "C" void c_assert(const char *assertion, const char *file, int line)
{
#ifdef QT_NO_DEBUG
    qDebug() << QString("ASSERT(%1) in file '%2' (line %3)").arg(assertion).arg(file).arg(line);
#else
    qt_assert(assertion, file, line);
#endif
}

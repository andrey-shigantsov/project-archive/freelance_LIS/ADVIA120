#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QDateTime>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    Settings("advia120.conf",QSettings::IniFormat),
    wConnector(this),
    Mode(&ifCOM, TLisMode::DownloadingMode)
{
    ui->setupUi(this);
    ui->mainToolBar->setVisible(false);
    ui->statusBar->addPermanentWidget(statusConnect = new QLabel());
    ui->statusBar->addPermanentWidget(statusMode = new QLabel());
    set_ui_disconnected();

    switch (Settings.value("general/Mode").toChar().toAscii())
    {
    default:
    case 'D':
        setMode(TLisMode::DownloadingMode);
        break;

    case 'Q':
        setMode(TLisMode::QueryMode);
        break;

    case 'F':
        setMode(TLisMode::FullQueryMode);
        break;
    }

    wrkFilePath = Settings.value("general/WorkordersDir").toString();
    resFilePath = Settings.value("general/ResultsDir").toString();

    wConnector.setInterface(&ifCOM);
    lisFile.setFilePath(resFilePath);

    statusBarTimeout = 30000;

    connect(&ifCOM, SIGNAL(ConnectStatus_changed(TLisInterface::ConnectStatus)), this, SLOT(com_connectstatus_handler(TLisInterface::ConnectStatus)));
    connect(&ifCOM, SIGNAL(error(QString,QString)), this, SLOT(error_handler(QString,QString)));

    connect(&Mode, SIGNAL(message(QString)), this, SLOT(show_message(QString)));
    connect(&Mode, SIGNAL(warning(QString,QString)), this, SLOT(warning_handler(QString,QString)));
    connect(&Mode, SIGNAL(error(QString,QString)), this, SLOT(error_handler(QString,QString)));
    connect(&Mode, SIGNAL(workorder_transmitted(QString,bool)), this, SLOT(set_workorder_res(QString,bool)));
    connect(&Mode, SIGNAL(pdu_ready(lis_ProtocolDataUnit_t)), this, SLOT(pdu_to_file(lis_ProtocolDataUnit_t)));

    connect(&wConnector, SIGNAL(error(QString,QString)), this, SLOT(error_handler(QString,QString)));

    connect(&lisFile, SIGNAL(warning(QString,QString)), this, SLOT(warning_handler(QString,QString)));
    connect(&lisFile, SIGNAL(error(QString,QString)), this, SLOT(error_handler(QString,QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::ModeName()
{
    return TLisMode::TypeName(modeType);
}

void MainWindow::setMode(TLisMode::Type mode)
{
    this->modeType = mode;
    switch(mode)
    {
    case TLisMode::DownloadingMode:
        Settings.setValue("general/Mode", "D");
        ui->action_Mode_D->setChecked(true);
        ui->action_Mode_Q->setChecked(false);
        ui->action_Mode_F->setChecked(false);
        break;

    case TLisMode::QueryMode:
        Settings.setValue("general/Mode", "Q");
        ui->action_Mode_D->setChecked(false);
        ui->action_Mode_Q->setChecked(true);
        ui->action_Mode_F->setChecked(false);
        break;

    case TLisMode::FullQueryMode:
        Settings.setValue("general/Mode", "F");
        ui->action_Mode_D->setChecked(false);
        ui->action_Mode_Q->setChecked(false);
        ui->action_Mode_F->setChecked(true);
        break;
    }
}

void MainWindow::set_ui_connected()
{
    ui->action_COM_Connect->setEnabled(false);
    ui->action_COM_disconnect->setEnabled(true);
    set_ui_mode_stopped();

    statusConnect->setText(wConnector.Port());
}

void MainWindow::set_ui_disconnected()
{
    ui->action_COM_Connect->setEnabled(true);
    ui->action_COM_disconnect->setEnabled(false);
    ui->action_StartMode->setEnabled(false);
    ui->action_StopMode->setEnabled(false);

    ui->menu_Mode->setEnabled(true);    

    statusConnect->setText(tr("Не подключено"));
    statusMode->setVisible(false);
}

void MainWindow::set_ui_mode_started()
{
    ui->menu_Mode->setEnabled(false);
    ui->action_StartMode->setEnabled(false);
    ui->action_StopMode->setEnabled(true);
    ui->action_WrkClear->setEnabled(false);
    statusMode->setVisible(true);
    statusMode->setText(ModeName());
}

void MainWindow::set_ui_mode_stopped()
{
    ui->menu_Mode->setEnabled(true);
    ui->action_StartMode->setEnabled(true);
    ui->action_StopMode->setEnabled(false);
    ui->action_WrkClear->setEnabled(true);
    statusMode->setVisible(false);
}

void MainWindow::procWorkorders(int startIndex)
{
    for(int i = startIndex; i < ui->Workorders->count(); ++i)
    {
        lis_Data_t data;
        bool res = lisFile.read(ui->Workorders->item(i)->text(), lisid_Workorder, &data);
        if (!res)
        {
            ui->Workorders->item(i)->setBackgroundColor(QColor(Qt::red));
            ui->Workorders->item(i)->setTextColor(QColor(Qt::white));
            continue;
        }
        ui->Workorders->item(i)->setBackgroundColor(QColor(Qt::darkGreen));
        ui->Workorders->item(i)->setTextColor(QColor(Qt::white));
        Mode.appendWorkorder(ui->Workorders->item(i)->text(), data.Workorder);
    }
}

void MainWindow::start_mode()
{
    procWorkorders();
    Mode.setType(modeType);
    Mode.start();

    set_ui_mode_started();
}

void MainWindow::stop_mode()
{
    Mode.stop();

    set_ui_mode_stopped();
}

void MainWindow::pdu_to_file(lis_ProtocolDataUnit_t pdu)
{
    lisFile.write(pdu.id, &pdu.data);
}

void MainWindow::set_workorder_res(QString filename, bool isValid)
{
    for(int i = 0; i < ui->Workorders->count(); ++i)
        if (ui->Workorders->item(i)->text() == filename)
        {
            if (isValid)
                ui->Workorders->takeItem(i);
            else
                ui->Workorders->item(i)->setBackgroundColor(QColor(Qt::darkYellow));
        }
}

void MainWindow::com_connectstatus_handler(TLisInterface::ConnectStatus status)
{
    switch(status)
    {
    case TLisInterface::Connected:
        set_ui_connected();
        show_message(tr("%1 порт открыт").arg(wConnector.Port()));
        break;

    case TLisInterface::Disconnected:
        set_ui_disconnected();
        show_message(tr("%1 порт закрыт").arg(wConnector.Port()));
        break;
    }
}

void MainWindow::warning_handler(QString src, QString msg)
{
    QString text = src + ": " + msg;
    log_op_append(text);
    qDebug() << text;
}

void MainWindow::error_handler(QString src, QString msg)
{
    QString text = src + ": " + msg;
    QMessageBox::warning(this, tr("Ошибка"), text);

    text = tr("!ОШИБКА! ") + text;
    log_op_append(text);
    qDebug() << text;

    stop_mode();
    show_message(tr("Сбой"));
    //wStatus->setMessage(tr("Сбой"));
}

void MainWindow::show_message(QString msg)
{
    log_op_append(msg);
    ui->statusBar->showMessage(msg, statusBarTimeout);
    //wStatus->setMessage(msg);
}

void MainWindow::log_op_append(QString msg)
{
    ui->OperationLog->append("(" + QDateTime::currentDateTime().toString(/*"dd.MM.yy""/"*/"hh:mm:ss") + ") " +
                             msg);
}

//-------------------------------------------------------

void MainWindow::on_action_COM_Connect_triggered()
{
    wConnector.refresh_ports();
    wConnector.show();
}

void MainWindow::on_action_COM_disconnect_triggered()
{
    stop_mode();
    ifCOM.close_connection();
}

void MainWindow::on_action_Mode_D_triggered()
{
    setMode(TLisMode::DownloadingMode);
}

void MainWindow::on_action_Mode_Q_triggered()
{
    setMode(TLisMode::QueryMode);
}

void MainWindow::on_action_Mode_F_triggered()
{
    setMode(TLisMode::FullQueryMode);
}

void MainWindow::on_action_StartMode_triggered()
{
    ifCOM.reset();
    start_mode();
}

void MainWindow::on_action_StopMode_triggered()
{
    stop_mode();
    ifCOM.reset();

    show_message(tr("остановлено пользователем"));
}

void MainWindow::on_action_WrkAdd_triggered()
{
    QStringList wrks = QFileDialog::getOpenFileNames(this, tr("Открыть задания"), wrkFilePath);
    int i = ui->Workorders->count();
    ui->Workorders->addItems(wrks);
    if (Mode.isStarted())
        procWorkorders(i);
}

void MainWindow::on_action_FilePath_Wrk_triggered()
{
    wrkFilePath = QFileDialog::getExistingDirectory(this, tr("Открыть папку с заданиями"), wrkFilePath);
    Settings.setValue("general/WorkordersDir", wrkFilePath);
}

void MainWindow::on_action_FilePath_Res_triggered()
{
    resFilePath = QFileDialog::getExistingDirectory(this, tr("Открыть папку с результатами"), resFilePath);
    Settings.setValue("general/ResultsDir", resFilePath);
    lisFile.setFilePath(resFilePath);
}

void MainWindow::on_action_WrkClear_triggered()
{
    ui->Workorders->clear();
}

void MainWindow::on_action_SaveSettings_triggered()
{
    Settings.sync();
}

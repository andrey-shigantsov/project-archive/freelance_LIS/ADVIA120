#ifndef TCONNECTORCOM_H
#define TCONNECTORCOM_H

#include <QWidget>
#include "LIS/TLisInterfaceCom.h"

namespace Ui {
class Connector;
}

class Connector : public QWidget
{
    Q_OBJECT

public:
    explicit Connector(QWidget *parent = 0);
    ~Connector();

    void refresh_ports();
    void setInterface(TLisInterfaceCom* p){interface = p;}

    QString Port();
    quint32 BaudRate();
    QSerialPort::DataBits DataBits();
    QSerialPort::Parity Parity();
    QSerialPort::StopBits StopBits();
    QSerialPort::FlowControl FlowControl();

private slots:
    void on_Connect_clicked();

private:
    Ui::Connector *ui;
    TLisInterfaceCom* interface;

signals:
    void error(QString src, QString msg);
};

#endif // TCONNECTORCOM_H

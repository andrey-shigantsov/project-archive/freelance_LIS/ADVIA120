#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QLabel>

#include "LIS/TLisInterfaceCom.h"
#include "Connector.h"

#include "LIS/TLisMode.h"

#include "LIS/TLisFile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString ModeName();
    void setMode(TLisMode::Type modeType);
    void setWorkordersDir(QString dir){wrkFilePath = dir;}
    void setResultsDir(QString dir){resFilePath = dir;}

private:
    Ui::MainWindow *ui;
    QSettings Settings;

    QLabel *statusConnect, *statusMode;

    int statusBarTimeout;

    TLisInterfaceCom ifCOM;
    Connector wConnector;

    TLisMode::Type modeType;
    TLisMode Mode;

    TLisFile lisFile;
    QString wrkFilePath, resFilePath;

    void set_ui_connected();
    void set_ui_disconnected();

    void set_ui_mode_started();
    void set_ui_mode_stopped();

    void start_mode();
    void stop_mode();

    void procWorkorders(int startIndex = 0);

private slots:
    void pdu_to_file(lis_ProtocolDataUnit_t pdu);
    void set_workorder_res(QString filename, bool isValid);
    void com_connectstatus_handler(TLisInterface::ConnectStatus status);
    void warning_handler(QString src, QString msg);
    void error_handler(QString src, QString msg);
    void show_message(QString msg);
    void log_op_append(QString msg);
//-------------------------------------------------------
    void on_action_COM_Connect_triggered();
    void on_action_COM_disconnect_triggered();
    void on_action_Mode_D_triggered();
    void on_action_Mode_Q_triggered();
    void on_action_Mode_F_triggered();
    void on_action_StartMode_triggered();
    void on_action_StopMode_triggered();
    void on_action_WrkAdd_triggered();
    void on_action_FilePath_Wrk_triggered();
    void on_action_FilePath_Res_triggered();
    void on_action_WrkClear_triggered();
    void on_action_SaveSettings_triggered();
};

#endif // MAINWINDOW_H
